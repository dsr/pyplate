import re
from pyExcelerator import *
import os
import sys
import csv
import wx
from FileMerger import csvMerger
import epace_browser


commas = re.compile(",")
frame_size = (550, 650)  # size of the frame
list_o_files = []  # list of files worked on generated by process()
input_files = []  # reports on replate activities
output_files = []  # 'replated_' + name of files in input files
input_output = {}  # dict to contain the input and output file names
presses = ['HARRIS - Harris M1000', 'SFMOAQ - Heidelberg MO2AQ', \
        'SFMO - Heidelberg MO1', 'DUQM02 - Quickmaster', 'CD74 - Speedmaster']


def say_done():
    done_dlg = wx.MessageDialog(None, "All Done", "All Done", \
            wx.OK | wx.ICON_INFORMATION)
    done_dlg.ShowModal()
    done_dlg.Destroy()


def say_error(error):
    """Provide message with error"""
    error_dlg = wx.MessageDialog(None, error, "Error", \
        wx.OK | wx.ICON_INFORMATION)
    error_dlg.ShowModal()
    error_dlg.Destroy()


def process_files(evt):
    """Create a dialog, process files contained in the selected directory"""
    dlg = wx.DirDialog(None, message="Choose a directory", \
            style=wx.OPEN | wx.CHANGE_DIR)
    if dlg.ShowModal() == wx.ID_OK:
        id = evt.GetId()
        
        doc = dlg.GetPath()
        if id == 30:
            epace_browser.get_reports(doc)
        else:
            pass
        fnames = os.listdir(doc)
        fnames.sort()
        ### Run Excel files through pyExcelerator. This is important as it
        ### generates the list for replating reports.
        for fname in fnames:
            parts = fname.split(".")
            if parts[-1] != "xls":
                continue
            process(os.path.join(doc, fname))
        if id == 10:
            ### Create the costed+plated.csv file
            matcher = MatchFinder('costing', doc, 'costed.csv', 'costed+plated.csv')
            if matcher.error:
                say_error(matcher.error)
            else:
                say_done()
            #MatchFinder('costing', doc, 'costed.csv', 'costed+plated.csv')
            #say_done()

        elif id == 20:
            ### Process paper files
            matcher = MatchFinder('paper', doc, 'costed.csv', 'paper_totals.csv')
            if matcher.error:
                say_error(matcher.error)
            else:
                say_done()

            #say_done()

        elif id == 30:
            # epace_browser.get_reports(doc)
            ### Process replate files.
            ### We'll iterate over the list of files created by process(),
            ### get the file names and then add that to a list.
            #TODO: This kinda' sucks. Right now all the replate reports have
            #       to start as .xls files.

            for f in list_o_files:
                file_name = f.split('/')[-1]
                input_files.append(file_name)

            ### Append output file names
            output_files = ['replated_' + x for x in input_files]

            ### Put the input and output file names in a nice dict
            input_output = dict(zip(input_files, output_files))

            ### Get replates for each press and each type of replate
            for i, o in input_output.iteritems():
                MatchFinder('plating', doc, i, o)

            say_done()

        elif id == 40:
            ### Merge csv files in the selected directory
            path = dlg.GetPath()
            csvMerger(path, 'combined.csv', ['Description', 'Job Part'])
            say_done()

    dlg.Destroy()


class MatchFinder(object):
    """
    Find matches between the first column of two files and write matching rows
    from the second file.
    csv_file1 is used only for the first column of data and determines
    what the second file will be.
    """
    def __init__(self, type, directory, csv_file1, output_file_name):
        self.error = ""
        ### Read the first csv file
        try:
            self.file1 = list(csv.reader(open(os.path.join(directory, csv_file1), 'rU')))
            if type in ['plating', 'paper']:
                if type == 'plating':

                    self.job_notes = []
                    for row in self.file1:
                        self.job_notes.append([row[0], row[9]])
                self.file1_column1 = [row[0] for row in self.file1]
                if type == 'paper':
                    ### Remove "Job ()" from the first column leaving just numbers
                    processed_list = []
                    for item in self.file1_column1:
                        stripped_item = item.replace("Job (", "").replace(")", "")
                        processed_list.append(stripped_item)
                        # item_to_list = list(item)
                        # sliced_item = item_to_list[5:-1]
                        # joined_item = "".join(sliced_item)
                        # processed_list.append(joined_item)
                    self.file1_column1 = processed_list
            else:
                self.required_plates = [line for line in self.file1 \
                        if line[10] in presses]

                self.file1_column1 = [row[0] for row in self.required_plates]
        except IOError:
            self.error = "Couldn't find {0}".format(csv_file1)
            return

            ### Assign the correct csv_file2 based on type
        if type == 'plating':
            csv_file2 = 'costed+plated.csv'
        elif type == 'paper':
            csv_file2 = 'paper.csv'
        elif type == 'costing':
            csv_file2 = 'plated.csv'
        else:
            pass
        ### Read the second csv file
        try:
            self.file2 = list(csv.reader(open(os.path.join(directory, csv_file2), 'rU')))
        except IOError:
            self.error = "Couldn't find {0}".format(csv_file2)
            return
        ### create list of results comparing first column of file1 with file2

        self.matched = [x for x in self.file2 for y in self.file1_column1 if \
                        x[0] == y]
        # for item in self.matched:
        #             print item
        self.matched_column1 = [row[0] for row in self.matched]

        ### Get the job numbers that didn't match
        if type == 'paper':
            self.not_matched = [job_number for job_number in \
                    processed_list if \
                    job_number not in self.matched_column1]
        else:
            self.not_matched = [job_number for job_number in \
                    self.file1_column1 if job_number not in \
                    self.matched_column1]
        ### Create the output file
        self.output_file = open(os.path.join(directory, output_file_name), 'wb')
        ### Create a csv file writer to writer to output file
        self.file_writer = csv.writer(self.output_file)
        ### Write the matched items to file
        for line in self.matched:
            self.file_writer.writerow(line)
        ### Write the unmatched items to file
        self.file_writer.writerow(["Not Matched"] + self.not_matched)
        if type == 'plating':
            for line in self.job_notes:
                self.file_writer.writerow(line)
        self.output_file.close()


def process(fname):
    """This function, slightly modified, is borrowed from the pyExcelerator \
            examples"""
    if fname[-4:] != ".xls":
        fname = fname + ".xls"
    #print "processing", fname
    # parse_xls(arg) -- default encoding
    for sheet_name, values in parse_xls(fname, 'cp1251'):
        #print "     starting", sheet_name,
        keys = values.keys()
        rows = []
        cols = []
        for key in keys:
            row, col = key
            if not col in cols:
                cols.append(col)
            if not row in rows:
                rows.append(row)
        try:
            n_rows = max(rows)
        except:
            #print "which is null."
            continue
        n_cols = max(cols)
        #print "which has", n_rows+1, "rows, and", n_cols+1, "columns."
        # ofile = open(fname + "." + sheet_name + ".csv",     "w")
        # modified to omit sheet name
        ofile = open(fname + ".csv",     "w")

        for row in range(n_rows + 1):
            line = ""
            for col in range(n_cols + 1):
                try:
                    cell = str(values[(row, col)])
                except:
                    cell = ""
                if commas.search(cell) != None:
                    cell = '"' + cell + '"'
                line = line + cell + ","
            print >> ofile, line[:-1]

        ofile.close()
        list_o_files.append(fname + '.csv')
    #print '----------------'


### Begin wxpython GUI
class MyFrame(wx.Frame):
    """ Frame to hold the app """
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(frame_size))
        self.SetMinSize(frame_size)
        self.SetMaxSize(frame_size)

        self.menubar = wx.MenuBar()
        self.file_menu = wx.Menu()
        #self.file_menu.Append(101, text = "Open\tCtrl+O")
        #self.file_menu.Append(103, text = "Export\tCtrl+E")
        self.file_menu.Append(102, text="Quit\tCtrl+Q")
        self.about_menu = wx.Menu()
        self.about_menu.Append(wx.ID_ABOUT, "&About PyPlate")
        self.menubar.Append(self.file_menu, "&File")
        self.menubar.Append(self.about_menu, "&Help")
        self.SetMenuBar(self.menubar)
        self.button_plates = wx.Button(self, 10, label="Costed/Plated")
        self.button_paper = wx.Button(self, 20, label="Paper")
        self.button_replates = wx.Button(self, 30, label="Replates")
        self.button_combine_csvs = wx.Button(self, 40, \
        label="Combine CSV Files")

        self.plate_text = wx.StaticText(self, -1, \
                "Requires: costed.csv, plated.csv")
        self.paper_text = wx.StaticText(self, -1, \
                "Requires: costed.csv, paper.csv")
        self.replate_text = wx.StaticText(self, -1, \
                "Requires replating reports")
        self.instructions = wx.StaticText(self, -1, \
"""INSTRUCTIONS

Download costed jobs via Shipment Numbers 2 report. Rename to costed.csv
and put in current month's folder

Paper
1. Download previous two month's paper reports via
   Estimated vs Pulled Paper, save as .csv in Paper Reports
2. Combine previous months' into paper.csv and move to
   current month's folder
3. Create paper_totals.csv with PyPlate
4. Look at "Not Matched" jobs and append paper_totals.csv
5. Sort and separate by press
6. Save final version as .xls or you'll loose the sheet for
   each press

Plates:
1. Pull this month and last month's Planned vs Pulled
2. Combine previous 2 months with this month's plates and save as
   plated.csv in current month's folder
3. Create costed+plated.csv with PyPlate
4. Download each replate report (leave in .xls format)
""")
        self.hsizer_1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hsizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hsizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hsizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hsizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.hsizer_1.Add(self.button_plates, 0, wx.ALL, border=10)
        self.hsizer_1.Add(self.plate_text, 0, wx.ALL, border=10)
        self.hsizer_2.Add(self.button_paper, 0, wx.ALL, border=10)
        self.hsizer_2.Add(self.paper_text, 0, wx.ALL, border=10)
        self.hsizer_3.Add(self.button_replates, 0, wx.ALL, border=10)
        self.hsizer_3.Add(self.replate_text, 0, wx.ALL, border=10)
        self.hsizer_4.Add(self.button_combine_csvs, 0, wx.ALL, border=10)
        self.hsizer_5.Add(self.instructions, 0, wx.ALL, border=10)

        self.sizer.Add(self.hsizer_1)
        self.sizer.Add(self.hsizer_2)
        self.sizer.Add(self.hsizer_3)
        self.sizer.Add(self.hsizer_4)
        self.sizer.Add(self.hsizer_5)

        self.Bind(wx.EVT_BUTTON, process_files, self.button_plates)
        self.Bind(wx.EVT_BUTTON, process_files, self.button_paper)
        self.Bind(wx.EVT_BUTTON, process_files, self.button_replates)
        self.Bind(wx.EVT_BUTTON, process_files, self.button_combine_csvs)
        self.Bind(wx.EVT_MENU, self.menu101, id=101)
        self.Bind(wx.EVT_MENU, self.menu102, id=102)
        self.Bind(wx.EVT_MENU, self.menu103, id=103)
        self.Bind(wx.EVT_MENU, self.show_about, id=wx.ID_ABOUT)
        self.SetSizer(self.sizer)

        self.Show(True)

    def show_about(self, evt):
        """docstring for about_menu"""
        about = wx.AboutDialogInfo()
        about.Name = "PyPlate"
        about.Version = "1.5"
        about.Description = "For processing ePace files for montly reports"
        about.Copyright = "(C) 2011 Dan Ross Demco Printing"
        wx.AboutBox(about)

    def menu101(self, evt):
        self.OnButton_button_add(evt)

    def menu102(self, e):
        sys.exit()

    def menu103(self, e):
        dlg = wx.FileDialog(self, style=wx.SAVE)
        if dlg.ShowModal() == wx.ID_OK:
            text = self.results_box.GetParser().GetSource()
            path = dlg.GetPath()
            f = open(path, 'a')
            f.write(text)
            f.close()


app = wx.App(False)
frame = MyFrame(None, 'PyPlate')
app.MainLoop()
