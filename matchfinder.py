class MatchFinder(object):
    """
    Find matches between the first column of two files and write matching rows
        from the second file
    csv_file1 is used only for the first column of data and determines
        what the second file will be
    """
    def __init__(self, type, directory, csv_file1, output_file_name):
        self.error = ""
        ### Read the first csv file
        self.file1 = csv.reader(open(os.path.join(directory, csv_file1), 'rU'))
       
        if type in ['plating', 'paper']:
            self.file1_column1 = [row[0] for row in self.file1]
            if type == 'paper':
                
                ### Remove "Job ()" from the first column leaving just numbers 
                processed_list = []
                for item in self.file1_column1:
                    stripped_item = item.replace("Job (", "").replace(")","")
                    processed_list.append(stripped_item)
                self.file1_column1 = processed_list
        else:
            self.required_plates = [line for line in self.file1 \
                    if line[10] in presses]
            
            self.file1_column1 = [row[0] for row in self.required_plates]
            
            
        ### Assign the correct csv_file2 based on type
        if type == 'plating':
            csv_file2 = 'costed+plated.csv'
        elif type == 'paper':
            csv_file2 = 'paper.csv'
        elif type == 'costing':
            csv_file2 = 'plated.csv'            
        else: 
            pass
        
        ### Read the second csv file    
        self.file2 = csv.reader(open(os.path.join(directory, csv_file2), 'rU'))
        
        ### create list of results comparing first column of file1 with file2
        
        #TODO: For replating, it'd be nice to add the notes to the results
        self.matched = [x for x in self.file2 for y in self.file1_column1 if \
                        x[0] == y]
        # for item in self.matched:
        #             print item
        self.matched_column1 = [row[0] for row in self.matched]
        
        ### Get the job numbers that didn't match    
        if type == 'paper':
            self.not_matched = [job_number for job_number in \
                    processed_list if \
                    job_number not in self.matched_column1]
        else:
            self.not_matched = [job_number for job_number in \
                    self.file1_column1 if job_number not in \
                    self.matched_column1]
        ### Create the output file
        self.output_file = open(os.path.join(directory, output_file_name), 'wb')
        
        ### Create a csv file writer to writer to output file
        self.file_writer = csv.writer(self.output_file)
        
        ### Write the matched items to file
        for line in self.matched:
            self.file_writer.writerow(line)
        
        ### Write the unmatched items to file
        self.file_writer.writerow(["Not Matched"] + self.not_matched)
        
        self.output_file.close()
