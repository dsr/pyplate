import os
import csv

class csvMerger(object):
    """
    Take a directory of csv files and merge them into one file. The file will
    be saved in the same directory.
    A list of ignored_row_identifiers is supplied when object is created.
    The list is compared to the second cell of the header row.
    """
    
    def __init__(self, directory, file_out, ignored_row_identifiers):
        self.ignored_row_identifiers = ignored_row_identifiers
        self.directory = directory
        self.file_out = open(os.path.join(directory, file_out), 'a')
        self.csv_writer = csv.writer(self.file_out)
        self.paths = []
        self.ignored_files = ['.DS_Store', file_out]

        for r, d, f in os.walk(self.directory):
            self.root = r
            self.dirs = d
            self.files = f
            for file in self.files:
                if not file in self.ignored_files:
                    self.paths.append(os.path.join(r, file))
        
        for csv_file in self.paths:
            self.csv_reader = csv.reader(open(csv_file, 'rU'))
            for row in self.csv_reader:
                if row[1] not in self.ignored_row_identifiers:
                    self.csv_writer.writerow(row) 
        self.file_out.close()
    


#csvMerger("/Users/danr/Desktop/csv_test/", 'combined.csv',\
#        ['Description', 'Job Part'])
