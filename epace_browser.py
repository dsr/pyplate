import mechanize
from credentials import credentials
import os

report_urls = {'harris_prepress':"http://epace/epace/inquiry/UserDefinedInquiry/view/5138.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'harris_customer':"http://epace/epace/inquiry/UserDefinedInquiry/view/5137.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'harris_production':"http://epace/epace/inquiry/UserDefinedInquiry/view/5140.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'harris_equipment':"http://epace/epace/inquiry/UserDefinedInquiry/view/5139.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'mo_prepress':"http://epace/epace/inquiry/UserDefinedInquiry/view/5125.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'mo_customer':"http://epace/epace/inquiry/UserDefinedInquiry/view/5132.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'mo_production':"http://epace/epace/inquiry/UserDefinedInquiry/view/5133.xls?cocoon-view=xls&pageSize=10000&printable=true",
                'mo_equipment':"http://epace/epace/inquiry/UserDefinedInquiry/view/5131.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "qm_prepress":"http://epace/epace/inquiry/UserDefinedInquiry/view/5135.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "qm_customer":"http://epace/epace/inquiry/UserDefinedInquiry/view/5134.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "qm_equipment":"http://epace/epace/inquiry/UserDefinedInquiry/view/5136.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "qm_production":"http://epace/epace/inquiry/UserDefinedInquiry/view/5141.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "cd_customer":"http://epace/epace/inquiry/UserDefinedInquiry/view/5159.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "cd_equipment":"http://epace/epace/inquiry/UserDefinedInquiry/view/5162.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "cd_prepress":"http://epace/epace/inquiry/UserDefinedInquiry/view/5160.xls?cocoon-view=xls&pageSize=10000&printable=true",
                "cd_production":"http://epace/epace/inquiry/UserDefinedInquiry/view/5161.xls?cocoon-view=xls&pageSize=10000&printable=true",
                }

def get_reports(directory):
    print("Getting reports")
    # Create browser and log in #
    br = mechanize.Browser()
    epace = br.open("http://epace/epace/login.html")
    # print(epace.read())
    br.select_form(nr=0)
    br.form['j_username'] = credentials['username']
    br.form['j_password'] = credentials['password']
    br.submit()
    try:
        for k, v in report_urls.iteritems():
            local_file = open(os.path.join(directory, "{0}.xls".format(k)), "w")
            local_file.write(br.open(v).read())
            local_file.close()
    except:
        br.open("http://epace/epace/do-logout")

    br.open("http://epace/epace/do-logout")


# get_reports()